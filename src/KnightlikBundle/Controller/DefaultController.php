<?php

namespace KnightlikBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->render('KnightlikBundle:Default:index.html.twig');

    }

    /**
     * @Route("/addTeam")
     */
    public function addTeam()
    {
        return $this->render('KnightlikBundle:Default:addTeam.html.twig');
    }

    /**
     * @Route("/editTeam")
     */
    public function editTeam()
    {
        return $this->render('KnightlikBundle:Default:editTeam.html.twig');
    }


}
