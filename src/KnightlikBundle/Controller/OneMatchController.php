<?php

namespace KnightlikBundle\Controller;

use KnightlikBundle\Entity\Tournament;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use KnightlikBundle\Entity\OneMatch;
use KnightlikBundle\Form\OneMatchType;

/**
 * OneMatch controller.
 *
 * @Route("/onematch")
 */
class OneMatchController extends Controller
{
    /**
     * Lists all OneMatch entities.
     *
     * @Route("/", name="onematch_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $oneMatches = $em->getRepository('KnightlikBundle:OneMatch')->findAll();

        return $this->render('onematch/index.html.twig', array(
            'oneMatches' => $oneMatches,
        ));
    }



    /**
     * Lists one OneMatch entities from tournament.
     *
     * @Route("/{id}", name="allmatches_tournament")
     * @Method("GET")
     */
    public function allMatchesFromTournamentAction(Tournament $tournament)
    {
        $em = $this->getDoctrine()->getManager();

        $oneMatches = $em->getRepository('KnightlikBundle:OneMatch')->findBy(
            ['tournamentId' => $tournament ]
        );

        return $this->render('onematch/index.html.twig', array(
            'oneMatches' => $oneMatches,
        ));
    }

    /**
     * Creates a new OneMatch entity.
     *
     * @Route("/new", name="onematch_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $oneMatch = new OneMatch();
        $form = $this->createForm('KnightlikBundle\Form\OneMatchType', $oneMatch);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($oneMatch);
            $em->flush();

            return $this->redirectToRoute('onematch_show', array('id' => $oneMatch->getId()));
        }

        return $this->render('onematch/new.html.twig', array(
            'oneMatch' => $oneMatch,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a OneMatch entity.
     *
     * @Route("/{id}", name="onematch_show")
     * @Method("GET")
     */
    public function showAction(OneMatch $oneMatch)
    {
        $deleteForm = $this->createDeleteForm($oneMatch);

        return $this->render('onematch/show.html.twig', array(
            'oneMatch' => $oneMatch,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing OneMatch entity.
     *
     * @Route("/{id}/edit", name="onematch_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, OneMatch $oneMatch)
    {
        $deleteForm = $this->createDeleteForm($oneMatch);
        $editForm = $this->createForm('KnightlikBundle\Form\OneMatchType', $oneMatch);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($oneMatch);
            $em->flush();

            return $this->redirectToRoute('onematch_edit', array('id' => $oneMatch->getId()));
        }

        return $this->render('onematch/edit.html.twig', array(
            'oneMatch' => $oneMatch,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to add winner team an existing OneMatch entity.
     *
     * @Route("/{id}/add_winner", name="onematch_add_winner")
     * @Method({"GET", "POST"})
     */
    public function addWinnerAction(Request $request, OneMatch $oneMatch)
    {

        $addWinnerForm = $this->createForm('KnightlikBundle\Form\OneMatchAddWinnerType', $oneMatch);
        $addWinnerForm->handleRequest($request);

        if ($addWinnerForm->isSubmitted() && $addWinnerForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($oneMatch);
            $em->flush();
            if($oneMatch->getIncomingMatchId() == true){
                $this->addWinnerToNextMatch($oneMatch->getIncomingMatchId(),$oneMatch->getWinnerTeam());

            }


            return $this->redirectToRoute('onematch_add_winner', array('id' => $oneMatch->getId()));
        }

        return $this->render('onematch/add_winner.html.twig', array(
            'oneMatch' => $oneMatch,
            'addWinner_form' => $addWinnerForm->createView(),

        ));
    }

    /**
     * Deletes a OneMatch entity.
     *
     * @Route("/{id}", name="onematch_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, OneMatch $oneMatch)
    {
        $form = $this->createDeleteForm($oneMatch);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($oneMatch);
            $em->flush();
        }

        return $this->redirectToRoute('onematch_index');
    }

    /**
     * Add winner team to next phase
     * @param
     */
    public function addWinnerToNextMatch($incomingMatchId,$winnerTeam)
    {
        $em = $this->getDoctrine()->getManager();

        $oneMatch = $em->find('KnightlikBundle:OneMatch',$incomingMatchId);
        if ($oneMatch->getIdTeam1() == null) {
            $oneMatch->setIdTeam1($winnerTeam);
        }
        else if ($oneMatch->getIdTeam2() == null) {
            $oneMatch->setIdTeam2($winnerTeam);
        }

        $em->persist($oneMatch);
        $em->flush();
        dump($oneMatch);

    }

    /**
     * Creates a form to delete a OneMatch entity.
     *
     * @param OneMatch $oneMatch The OneMatch entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(OneMatch $oneMatch)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('onematch_delete', array('id' => $oneMatch->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }



}
