<?php

namespace KnightlikBundle\Controller;

use Doctrine\Common\Persistence\PersistentObject;
use KnightlikBundle\Entity\OneMatch;
use KnightlikBundle\Form\TournamentAddTeamType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use KnightlikBundle\Entity\Tournament;
use KnightlikBundle\Form\TournamentType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Tournament controller.
 *
 * @Route("/tournament")
 */
class TournamentController extends Controller
{
    /**
     * Lists all Tournament entities.
     *
     * @Route("/all/{page}",
     *      name="tournament_index",
     *      defaults = {"page" =1},
     *      requirements={"page": "\d+"})
     *
     */
    public function indexAction(Request $request, $page)
    {
        $em = $this->getDoctrine()->getManager();

        $tournaments = $em->getRepository('KnightlikBundle:Tournament')->findAll();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $tournaments, /* query NOT result */
            $page/*page number*/,
            5/*limit per page*/
        );
        return $this->render('tournament/index.html.twig', array(
            'pagination' => $pagination
        ));
    }

    /**
     * Creates a new Tournament entity.
     *
     * @Route("/new", name="tournament_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $tournament = new Tournament();
        $form = $this->createForm('KnightlikBundle\Form\TournamentType', $tournament);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $tournament->setIsStarted(false);
            $em->persist($tournament);
            $em->flush();

            return $this->redirectToRoute('tournament_index');

        }

        return $this->render('tournament/new.html.twig', array(
            'tournament' => $tournament,
            'form' => $form->createView(),
        ));
    }

    /**
     * Change isStarted tournament on true.
     *
     * @Route("/{id}/start", name="tournament_start")
     * @Method({"GET", "POST"})
     */
    public function startAction(Request $request, Tournament $tournament)
    {

        $em = $this->getDoctrine()->getManager();
        $tournament->setIsStarted(true);

        $startPhase=log((int)$tournament->getTeamAmount())/log(2);

        $this->generateMatches($tournament, $startPhase);
        $em->persist($tournament);
        $em->flush();

        return $this->redirectToRoute('tournament_index');
    }

    /**
     * Finds and displays a Tournament entity.
     *
     * @Route("/{id}", name="tournament_show")
     * @Method("GET")
     */
    public function showAction(Tournament $tournament)
    {
        $deleteForm = $this->createDeleteForm($tournament);


        return $this->render('tournament/show.html.twig', array(
            'tournament' => $tournament,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Show tree tournament
     *
     * @Route("/tree/{id}", name="tournament_tree")
     *
     */
    public function showTreeAction(Tournament $tournament)
    {
        $matches = $tournament->getMatches();

        return $this->render('tournament/tree.html.twig', array(
            'matches' => $matches,
        ));
    }




    /**
     * Displays a form to edit an existing Tournament entity.
     *
     * @Route("/{id}/edit", name="tournament_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Tournament $tournament)
    {
        $deleteForm = $this->createDeleteForm($tournament);
        $editForm = $this->createForm('KnightlikBundle\Form\TournamentType', $tournament);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tournament);
            $em->flush();

            return $this->redirectToRoute('tournament_edit', array('id' => $tournament->getId()));
        }

        return $this->render('tournament/edit.html.twig', array(
            'tournament' => $tournament,
            'edit_form' => $editForm->createView(),
            'add_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Tournament entity.
     *
     * @Route("/{id}", name="tournament_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Tournament $tournament)
    {
        $form = $this->createDeleteForm($tournament);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tournament);
            $em->flush();
        }

        return $this->redirectToRoute('tournament_index');
    }

    /**
     * Creates a form to delete a Tournament entity.
     *
     * @param Tournament $tournament The Tournament entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Tournament $tournament)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tournament_delete', array('id' => $tournament->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Add Team in tournament
     *
     * @Route("/{id}/add_teams_in_tournament", name="add_teams_in_tournament")
     */
    public function addTeamsInTournament(Request $request, Tournament $tournament){

        if ($tournament->getIsStarted() == TRUE)
        {
            throw new NotFoundHttpException("Turniej w toku");
        }
        $form = $this->createForm('KnightlikBundle\Form\TournamentAddTeamType', $tournament);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tournament);
            $em->flush();

            return $this->redirectToRoute('tournament_index');

        }

        return $this->render('tournament/new.html.twig', array(
            'tournament' => $tournament,
            'form' => $form->createView(),
        ));

    }

    /**
     *
     * @param int $phase
     * @param null $incomingMatchId
     */
    public function generateMatches($tournament, $phase=0, $incomingMatchId=null)
    {
        if ($phase <= 0) {
            return;
        }
        else {

            $oneMatch = new OneMatch();
            $oneMatch->setPhase($phase);
            $oneMatch->setTournamentId($tournament);
            $oneMatch->setIncomingMatchId($incomingMatchId);
            $em = $this->getDoctrine()->getManager();
            $em->persist($oneMatch);
            $em->flush();
            $phase--;
            $this->generateMatches($tournament, $phase, $oneMatch->getId());
            $this->generateMatches($tournament, $phase, $oneMatch->getId());

        }
    }

    /**
     * Set teams to matches where phase is 1
     * @Route("/{id}/add_teams_to_matches", name = "add_teams_to_matches")
     */
    public function addTeamsToMatches(Tournament $tournament)
    {
        $teams = $tournament->getTeams();

        $team = $teams->current();

        $em = $this->getDoctrine()->getManager();

        $repository = $this->getDoctrine()->getRepository('KnightlikBundle:OneMatch');

        $matches = $repository->findBy(
            array('tournamentId' => $tournament->getId(),
            'phase' => 1));
     //   dump($matches);
        foreach($matches as $match)
        {

                $match->setIdTeam1($team);
            $team = $teams->next();
            $match->setIdTeam2($team);
           $team = $teams->next();
            dump($team);
                $em->persist($match);
                $em->flush();
        }

        return $this->redirectToRoute('tournament_index');
    }

//    /**
//     * Set teams to matches where phase is 2
//     * @Route("/{id}/add_teams_to_matches", name = "add_teams_to_matches")
//     */
//    public function addTeamsToMatches(Tournament $tournament)
//    {
//        $teams = $tournament->getTeams();
//
//        $team = $teams->current();
//
//        $em = $this->getDoctrine()->getManager();
//
//        $repository = $this->getDoctrine()->getRepository('KnightlikBundle:OneMatch');
//
//        $matches = $repository->findBy(
//            array('tournamentId' => $tournament->getId(),
//                'phase' => 1));
//        dump($matches);
//        foreach($matches as $match)
//        {
//
//            $match->setIdTeam1($team);
//            $team = $teams->next();
//            $match->setIdTeam2($team);
//            $team = $teams->next();
//            dump($team);
//            $em->persist($match);
//            $em->flush();
//        }
//
//        return ;
//    }
}
