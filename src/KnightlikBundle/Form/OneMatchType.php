<?php

namespace KnightlikBundle\Form;

use KnightlikBundle\Entity\Team;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OneMatchType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('winnerTeam')
        //, EntityType::class, array(
               //  query choices from this entity
          //      'class' => Team::class,
             //    use the User.username property as the visible option string
            //    'choice_label' => 'id',
            //    ))
            ->add('incomingMatchId')
            ->add('phase')
            ->add('idTeam1')
            ->add('idTeam2')
         //   ->add('tournamentId')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'KnightlikBundle\Entity\OneMatch'
        ));
    }
}
