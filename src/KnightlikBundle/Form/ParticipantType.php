<?php

namespace KnightlikBundle\Form;

use KnightlikBundle\Entity\Team;
use KnightlikBundle\KnightlikBundle;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ParticipantType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName')
            ->add('lastName')
            ->add('nickName')
            ->add('role',ChoiceType::class, array('choices'=>array('captain'=>'1','player'=>'2','reserve'=>'3')))
            ->add('teamId', EntityType::class, array('class' => 'KnightlikBundle:Team', 'choice_label' => 'name', ) )
            ->add('save', SubmitType::class)
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'KnightlikBundle\Entity\Participant'
        ));
    }
}
