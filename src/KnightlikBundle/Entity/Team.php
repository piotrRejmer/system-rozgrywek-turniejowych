<?php

namespace KnightlikBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Team
 *
 * @ORM\Table(name="team")
 * @ORM\Entity(repositoryClass="KnightlikBundle\Repository\TeamRepository")
 */
class Team
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=60)
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 4,
     *      max = 30,
     *      minMessage = "Your name must be at least {{ limit }} characters long",
     *      maxMessage = "Your name cannot be longer than {{ limit }} characters"
     * )
     * @Assert\Regex(
     *     pattern="/\d/",
     *     match=false,
     *     message="Your name cannot contain a number"
     * )
     * @Assert\Regex(
     *     pattern="/^[A-Z]/",
     *     message="First letter should be big"
     * )
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=10)
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 3,
     *      max = 5,
     *      minMessage = "Your tag must be at least {{ limit }} characters long",
     *      maxMessage = "Your tag cannot be longer than {{ limit }} characters"
     * )
     * @Assert\Regex(
     *     pattern="/\d/",
     *     match=false,
     *     message="Your tag cannot contain a number"
     * )
     */
    protected $tag;

    /**
     * @var int
     * @Assert\NotBlank()
     * @ORM\Column(type="integer", length=1)
     */
    protected $level;

    /**
     *
     * @ORM\OneToMany(targetEntity="Participant", mappedBy="teamId",cascade={"all"})
     */
    protected $players;

    /**
     * @ORM\ManyToMany(targetEntity="Tournament", mappedBy="teams")
     */
    private $tournaments;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Team
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set tag
     *
     * @param string $tag
     *
     * @return Team
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set level
     *
     * @param integer $level
     *
     * @return Team
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer
     */
    public function getLevel()
    {
        return $this->level;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->players = new \Doctrine\Common\Collections\ArrayCollection();
    }

 

    /**
     * Add player
     *
     * @param \KnightlikBundle\Entity\Participant $player
     *
     * @return Team
     */
    public function addPlayer(\KnightlikBundle\Entity\Participant $player)
    {
        $this->players[] = $player;

        return $this;
    }

    /**
     * Remove player
     *
     * @param \KnightlikBundle\Entity\Participant $player
     */
    public function removePlayer(\KnightlikBundle\Entity\Participant $player)
    {
        $this->players->removeElement($player);
    }

    /**
     * Get players
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * Add tournament
     *
     * @param \KnightlikBundle\Entity\Tournament $tournament
     *
     * @return Team
     */
    public function addTournament(\KnightlikBundle\Entity\Tournament $tournament)
    {
        $this->tournaments[] = $tournament;

        return $this;
    }

    /**
     * Remove tournament
     *
     * @param \KnightlikBundle\Entity\Tournament $tournament
     */
    public function removeTournament(\KnightlikBundle\Entity\Tournament $tournament)
    {
        $this->tournaments->removeElement($tournament);
    }

    /**
     * Get tournaments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTournaments()
    {
        return $this->tournaments;
    }

    public function __toString() {
        return $this->name;
    }
}
