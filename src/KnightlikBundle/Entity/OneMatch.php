<?php

namespace KnightlikBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OneMatch
 *
 * @ORM\Table(name="one_match")
 * @ORM\Entity(repositoryClass="KnightlikBundle\Repository\OneMatchRepository")
 */
class OneMatch
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Team")
     * @ORM\JoinColumn(name="id_Team1", referencedColumnName="id")
     *
     */
    protected $idTeam1;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Team")
     * @ORM\JoinColumn(name="id_Team2", referencedColumnName="id")
     *
     */
    protected $idTeam2;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Team")
     * @ORM\JoinColumn(name="winner_team", referencedColumnName="id")
     *
     */
    protected $winnerTeam;

    /**
     * @var int
     *
     * @ORM\Column(name="incoming_match_id", type="integer", nullable=true, options={"default":null})
     */
    protected $incomingMatchId;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Tournament", inversedBy = "matches" )
     * @ORM\JoinColumn(name = "tournament_id", referencedColumnName = "id")
     */
    protected $tournamentId;

    /**
     * @var int
     *
     * @ORM\Column(name="phase", type="integer", nullable=true, options={"default":null})
     */
    protected $phase;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nextMatchId
     *
     * @param integer $nextMatchId
     *
     * @return OneMatch
     */
    public function setNextMatchId($nextMatchId)
    {
        $this->nextMatchId = $nextMatchId;

        return $this;
    }

    /**
     * Get nextMatchId
     *
     * @return int
     */
    public function getNextMatchId()
    {
        return $this->nextMatchId;
    }

    /**
     * Set winnerTeam
     *
     * @param integer $winnerTeam
     *
     * @return OneMatch
     */
    public function setWinnerTeam($winnerTeam)
    {
        $this->winnerTeam = $winnerTeam;

        return $this;
    }

    /**
     * Get winnerTeam
     *
     * @return integer
     */
    public function getWinnerTeam()
    {
        return $this->winnerTeam;
    }

    /**
     * Set incomingMatchId
     *
     * @param integer $incomingMatchId
     *
     * @return OneMatch
     */
    public function setIncomingMatchId($incomingMatchId)
    {
        $this->incomingMatchId = $incomingMatchId;

        return $this;
    }

    /**
     * Get incomingMatchId
     *
     * @return integer
     */
    public function getIncomingMatchId()
    {
        return $this->incomingMatchId;
    }

    /**
     * Set phase
     *
     * @param integer $phase
     *
     * @return OneMatch
     */
    public function setPhase($phase)
    {
        $this->phase = $phase;

        return $this;
    }

    /**
     * Get phase
     *
     * @return integer
     */
    public function getPhase()
    {
        return $this->phase;
    }

    /**
     * Set idTeam1
     *
     * @param \KnightlikBundle\Entity\Team $idTeam1
     *
     * @return OneMatch
     */
    public function setIdTeam1(\KnightlikBundle\Entity\Team $idTeam1)
    {
        $this->idTeam1 = $idTeam1;

        return $this;
    }

    /**
     * Get idTeam1
     *
     * @return \KnightlikBundle\Entity\Team
     */
    public function getIdTeam1()
    {
        return $this->idTeam1;
    }

    /**
     * Set idTeam2
     *
     * @param \KnightlikBundle\Entity\Team $idTeam2
     *
     * @return OneMatch
     */
    public function setIdTeam2(\KnightlikBundle\Entity\Team $idTeam2 = null)
    {
        $this->idTeam2 = $idTeam2;

        return $this;
    }

    /**
     * Get idTeam2
     *
     * @return \KnightlikBundle\Entity\Team
     */
    public function getIdTeam2()
    {
        return $this->idTeam2;
    }

    /**
     * Set tournamentId
     *
     * @param \KnightlikBundle\Entity\Tournament $tournamentId
     *
     * @return OneMatch
     */
    public function setTournamentId(\KnightlikBundle\Entity\Tournament $tournamentId = null)
    {
        $this->tournamentId = $tournamentId;

        return $this;
    }

    /**
     * Get tournamentId
     *
     * @return \KnightlikBundle\Entity\Tournament
     */
    public function getTournamentId()
    {
        return $this->tournamentId;
    }

    public function __toString() {
        return $this->name;
    }

}
