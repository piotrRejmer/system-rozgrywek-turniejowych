<?php

namespace KnightlikBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tournament
 *
 * @ORM\Table(name="tournament")
 * @ORM\Entity(repositoryClass="KnightlikBundle\Repository\TournamentRepository")
 */
class Tournament
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=40)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_started", type="boolean")
     */
    private $isStarted;


    /**
     * @var int
     *
     * @ORM\Column(name="team_amount",type="integer")
     */
    private $teamAmount;


    /**
     * @ORM\ManyToMany(targetEntity="Team", inversedBy="tournaments")
     * @ORM\JoinTable(name="tournaments_teams")
     */
    private $teams;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="OneMatch", mappedBy="tournamentId", cascade={"persist", "remove"})
     */
    private $matches;






    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Tournament
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set teams
     *
     * @param string $teams
     *
     * @return Tournament
     */
    public function setTeams($teams)
    {
        $this->teams = $teams;

        return $this;
    }



    /**
     * Get teams
     *
     * @return string
     */
    public function getTeams()
    {
        return $this->teams;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->teams = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add team
     *
     * @param \KnightlikBundle\Entity\Team $team
     *
     * @return Tournament
     */
    public function addTeam(\KnightlikBundle\Entity\Team $team)
    {
        $this->teams[] = $team;

        return $this;
    }

    /**
     * Remove team
     *
     * @param \KnightlikBundle\Entity\Team $team
     */
    public function removeTeam(\KnightlikBundle\Entity\Team $team)
    {
        $this->teams->removeElement($team);
    }

    /**
     * Set isStarted
     *
     * @param boolean $isStarted
     *
     * @return Tournament
     */
    public function setIsStarted($isStarted)
    {
        $this->isStarted = $isStarted;

        return $this;
    }

    /**
     * Get isStarted
     *
     * @return boolean
     */
    public function getIsStarted()
    {
        return $this->isStarted;
    }

    /**
     * Add match
     *
     * @param \KnightlikBundle\Entity\OneMatch $match
     *
     * @return Tournament
     */
    public function addMatch(\KnightlikBundle\Entity\OneMatch $match)
    {
        $this->matches[] = $match;

        return $this;
    }

    /**
     * Remove match
     *
     * @param \KnightlikBundle\Entity\OneMatch $match
     */
    public function removeMatch(\KnightlikBundle\Entity\OneMatch $match)
    {
        $this->matches->removeElement($match);
    }

    /**
     * Get matchs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMatches()
    {
        return $this->matches;
    }

    /**
     * Set teamAmount
     *
     * @param integer $teamAmount
     *
     * @return Tournament
     */
    public function setTeamAmount($teamAmount)
    {
        $this->teamAmount = $teamAmount;

        return $this;
    }

    /**
     * Get teamAmount
     *
     * @return integer
     */
    public function getTeamAmount()
    {
        return $this->teamAmount;
    }

    public function __toString() {
        return $this->name;
    }
}
