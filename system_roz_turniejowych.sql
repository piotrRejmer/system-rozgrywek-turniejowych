-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: system_rozgrywek_turniejowych
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `one_match`
--

DROP TABLE IF EXISTS `one_match`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_match` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tournament_id` int(11) DEFAULT NULL,
  `winner_team` int(11) DEFAULT NULL,
  `incoming_match_id` int(11) DEFAULT NULL,
  `phase` int(11) DEFAULT NULL,
  `id_Team1` int(11) DEFAULT NULL,
  `id_Team2` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_389D032A21546CD0` (`id_Team1`),
  KEY `IDX_389D032AB85D3D6A` (`id_Team2`),
  KEY `IDX_389D032A33D1A3E7` (`tournament_id`),
  CONSTRAINT `FK_389D032A21546CD0` FOREIGN KEY (`id_Team1`) REFERENCES `team` (`id`),
  CONSTRAINT `FK_389D032A33D1A3E7` FOREIGN KEY (`tournament_id`) REFERENCES `tournament` (`id`),
  CONSTRAINT `FK_389D032AB85D3D6A` FOREIGN KEY (`id_Team2`) REFERENCES `team` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_match`
--

LOCK TABLES `one_match` WRITE;
/*!40000 ALTER TABLE `one_match` DISABLE KEYS */;
INSERT INTO `one_match` VALUES (1,1,NULL,NULL,3,NULL,NULL),(2,1,NULL,1,2,NULL,NULL),(3,1,NULL,2,1,1,1),(4,1,NULL,2,1,1,1),(5,1,NULL,1,2,NULL,NULL),(6,1,NULL,5,1,1,1),(7,1,NULL,5,1,1,1),(8,1,NULL,NULL,3,NULL,NULL),(9,1,NULL,8,2,NULL,NULL),(10,1,NULL,9,1,1,1),(11,1,NULL,9,1,1,1),(12,1,NULL,8,2,NULL,NULL),(13,1,NULL,12,1,1,1),(14,1,NULL,12,1,1,1),(15,2,NULL,NULL,3,NULL,NULL),(16,2,NULL,15,2,NULL,NULL),(17,2,1,16,1,1,2),(18,2,3,16,1,3,4),(19,2,NULL,15,2,NULL,NULL),(20,2,NULL,19,1,5,6),(21,2,NULL,19,1,NULL,NULL),(22,2,NULL,NULL,3,NULL,NULL),(23,2,NULL,22,2,NULL,NULL),(24,2,NULL,23,1,1,2),(25,2,NULL,23,1,1,2),(26,2,NULL,22,2,NULL,NULL),(27,2,NULL,26,1,NULL,NULL),(28,2,NULL,26,1,NULL,NULL);
/*!40000 ALTER TABLE `one_match` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participant`
--

DROP TABLE IF EXISTS `participant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id_id` int(11) DEFAULT NULL,
  `first_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `nick_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `role` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D79F6B11B842D717` (`team_id_id`),
  CONSTRAINT `FK_D79F6B11B842D717` FOREIGN KEY (`team_id_id`) REFERENCES `team` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participant`
--

LOCK TABLES `participant` WRITE;
/*!40000 ALTER TABLE `participant` DISABLE KEYS */;
INSERT INTO `participant` VALUES (1,1,'Piotr','Rejmer','rejki',1);
/*!40000 ALTER TABLE `participant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team`
--

DROP TABLE IF EXISTS `team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `tag` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `level` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team`
--

LOCK TABLES `team` WRITE;
/*!40000 ALTER TABLE `team` DISABLE KEYS */;
INSERT INTO `team` VALUES (1,'Team','team',1),(2,'Druga','druga',1),(3,'Trzecia','trzec',1),(4,'Czwarta','czwar',1),(5,'Piata','piata',1),(6,'Szosta','szost',1);
/*!40000 ALTER TABLE `team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tournament`
--

DROP TABLE IF EXISTS `tournament`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tournament` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `is_started` tinyint(1) NOT NULL,
  `team_amount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tournament`
--

LOCK TABLES `tournament` WRITE;
/*!40000 ALTER TABLE `tournament` DISABLE KEYS */;
INSERT INTO `tournament` VALUES (1,'pierwszy',1,8),(2,'drugi',1,8);
/*!40000 ALTER TABLE `tournament` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tournaments_teams`
--

DROP TABLE IF EXISTS `tournaments_teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tournaments_teams` (
  `tournament_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  PRIMARY KEY (`tournament_id`,`team_id`),
  KEY `IDX_704D637C33D1A3E7` (`tournament_id`),
  KEY `IDX_704D637C296CD8AE` (`team_id`),
  CONSTRAINT `FK_704D637C296CD8AE` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_704D637C33D1A3E7` FOREIGN KEY (`tournament_id`) REFERENCES `tournament` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tournaments_teams`
--

LOCK TABLES `tournaments_teams` WRITE;
/*!40000 ALTER TABLE `tournaments_teams` DISABLE KEYS */;
INSERT INTO `tournaments_teams` VALUES (1,1),(1,2),(2,1),(2,2),(2,3),(2,4),(2,5),(2,6);
/*!40000 ALTER TABLE `tournaments_teams` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-02 11:49:47
